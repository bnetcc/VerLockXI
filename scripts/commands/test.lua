---------------------------------------------------------------------------------------------------
-- func: homepoint
-- desc: Sends the target to their homepoint.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "s"
};

function error(player, msg)
    player:PrintToPlayer(msg);
end;

function onTrigger(player, target)
target:PrintToPlayer("I see what you did there...", dsp.msg.channel.SAY, "[GM] Bot");
target:PrintToPlayer("I see what you did there...", dsp.msg.channel.TELL, "[GM] Bot");
target:PrintToPlayer("I see what you did there...", dsp.msg.channel.YELL, "[GM] Bot");
target:PrintToPlayer("I see what you did there...", dsp.msg.channel.SHOUT, "[GM] Bot");
end