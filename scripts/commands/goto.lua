---------------------------------------------------------------------------------------------------
-- func: goto
-- desc: Goes to the target player.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = "si"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!goto <player>");
end;

function onTrigger(player, target)

    -- validate target
    if (target == nil) then
        error(player, "You must enter a player name.");
        return;
    end
    local targ = GetPlayerByName( target );
    if (targ == nil) then
        if not player:gotoPlayer( target ) then
            error(player, string.format( "Player named '%s' not found!", target ) );
        end
        return;
    end

    -- validate forceZone

    -- goto target
    if (targ:getZoneID() ~= player:getZoneID() then
        player:setPos( targ:getXPos(), targ:getYPos(), targ:getZPos(), targ:getRotPos(), targ:getZoneID() );
    else
        player:setPos( targ:getXPos(), targ:getYPos(), targ:getZPos(), targ:getRotPos() );
    end
end